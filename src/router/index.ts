import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue')
      },
      meta: {
        layout: 'FullLayout',
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import("@/views/POS/POSView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import("@/views/Orders/OrderView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import("@/views/Products/ProductView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/employee',
      name: 'employee',
      components: {
        default: () => import("@/views/Employees/EmployeeView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/user',
      name: 'user',
      components: {
        default: () => import("@/views/Users/UserView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/store',
      name: 'store',
      components: {
        default: () => import("@/views/Stores/StoreView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: () => import("@/views/LoginView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'FullLayout',
      }
    },
    {
      path: '/material',
      name: 'material',
      components: {
        default: () => import("@/views/Materials/MaterialView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/customer',
      name: 'customer',
      components: {
        default: () => import("@/views/Customers/CustomerView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
    {
      path: '/category',
      name: 'category',
      components: {
        default: () => import("@/views/Categorys/CategoryView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue")
      },
      meta: {
        layout: 'MainLayout',
      }
    },
  ]
})

export default router
