import http from './axios'
import type Employee from '@/types/Employee'

function getEmployees() {
  return http.get("/employees")
}

function getEmployeeID(id: number) {
  return http.get(`/employees/${id}`)
}

function saveEmployee(employee: Employee) {
  return http.post("/employees", employee)
}

function updateEmployee(id: number, employee: Employee) {
  return http.patch(`/employees/${id}`, employee)
}

function deleteEmployee(id: number) {
  return http.delete(`/employees/${id}`)
}

export default { getEmployees, getEmployeeID, saveEmployee, deleteEmployee, updateEmployee }