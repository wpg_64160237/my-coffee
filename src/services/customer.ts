import http from './axios'
import type Customer from '@/types/Customer'

function getCustomers() {
  return http.get("/Customers")
}

function getCustomerID(id: number) {
  return http.get(`/Customers/${id}`)
}

function saveCustomer(Customer: Customer) {
  return http.post("/Customers", Customer)
}

function updateCustomer(id: number, Customer: Customer) {
  return http.patch(`/Customers/${id}`, Customer)
}

function deleteCustomer(id: number) {
  return http.delete(`/Customers/${id}`)
}

export default { getCustomers, getCustomerID, saveCustomer, deleteCustomer, updateCustomer }