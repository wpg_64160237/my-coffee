import http from './axios'
import type Store from '@/types/Store'

function getStores() {
  return http.get("/Stores")
}

function getStoreID(id: number) {
  return http.get(`/Stores/${id}`)
}

function saveStore(Store: Store) {
  return http.post("/Stores", Store)
}

function updateStore(id: number, Store: Store) {
  return http.patch(`/Stores/${id}`, Store)
}

function deleteStore(id: number) {
  return http.delete(`/Stores/${id}`)
}

export default { getStores, getStoreID, saveStore, deleteStore, updateStore }