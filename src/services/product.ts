import type Product from '@/types/Product'
import http from './axios'
function getProducts() {
  return http.get("/products")
}

function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("price", `${product.price}`);
  formData.append("size", product.size);
  formData.append("type", product.type);
  formData.append("category", product.category)
  formData.append("file", product.files[0]);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}

function updateProductImg(id: number, files: File[]) {
  const formDate = new FormData();
  formDate.append("file", files[0])
  return http.patch(`/products/${id}/image`, formDate, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
}

function updateProduct(id: number, product: Product) {
  return http.patch(`/products/${id}`, product)
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`)
}

export default { getProducts, saveProduct, updateProduct, deleteProduct, updateProductImg }