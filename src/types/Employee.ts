import type Order from "./Order";
import type User from "./User";

export default interface Employee {
  id?: number;
  name: string;
  tel: string;
  email: string;
  position: string;
  hourly_wage: number;
  user?: User;
  orders?: Order[];
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}