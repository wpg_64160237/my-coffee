import type Employee from "./Employee";

export default interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  role: string;
  employee: Employee;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}