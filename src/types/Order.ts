import type { Store } from "pinia";
import type Customer from "./Customer";
import type Employee from "./Employee";
import type OrderItem from "./OrderItem";

export default interface Order {
  id: number;
  amount: number;
  discount: number;
  total: number;
  recieved: number;
  changed: number;
  date: string;
  customer: Customer;
  employee: Employee;
  store: Store;
  orderItems: OrderItem;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}