import type OrderItem from "./OrderItem";

export default interface Product {
  id?: number;
  name: string;
  type: string;
  size: string;
  price: number;
  image?: string;
  category: string;
  orderItemsId?: OrderItem[];
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}