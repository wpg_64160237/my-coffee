import type Order from "./Order";

export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  point: number;
  orders?: Order[];
  startDate?: string;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}