import { computed, ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type Product from '@/types/Product';
import productService from '@/services/product'

export const useProductStore = defineStore('product', () => {
  const nav = ref(false);
  const search = ref("");
  const showProduct = computed(() => {
    return products.value.filter((product) => {
      // return product.name.toLowerCase().includes(search.value.toLowerCase());
      return (product.name || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product & { files: File[] }>({ name: "", type: "", size: "", price: 0, category: "", image: 'no_img_available.jpg', files: [] });

  watch(nav, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", type: "", size: "", price: 0, category: "", image: 'no_img_available.jpg', files: [] }
    }
  })

  async function getProducts() {
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e)
    }
  }

  async function saveProducts() {
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      nav.value = false;
      await getProducts();
    } catch (e) {
      console.log(e)
    }
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    nav.value = true;
  }

  async function deleteProduct(id: number) {
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e)
    }
  }

  const values = ref("เครื่องดื่ม");
  const showProductByCategory = computed(() => {
    return products.value.filter((item) => {
      return item.category.match(values.value);
    })
  });

  return { products, getProducts, nav, editedProduct, saveProducts, editProduct, deleteProduct, search, showProduct, values, showProductByCategory }
})
