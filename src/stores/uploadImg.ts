import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product';
import { useProductStore } from './product';


export const useuploadImgStore = defineStore('uploadImg', () => {
  const productStore = useProductStore();
  const files = ref<File[]>([]);
  const url = ref();
  const Id = ref();
  const service = ref("")
  const uploadImg = ref(false);
  watch(uploadImg, newuploadImg => {
    if (!newuploadImg) {
      files.value = [];
      Id.value = ref();
      service.value = "";
    }
  })

  function EditImg(id: number, services: string) {
    service.value = services
    Id.value = id;
    uploadImg.value = true;

  }
  function onFileChange() {
    const file = files.value[0]
    url.value = URL.createObjectURL(file);
    return files.value;
  }

  async function Sendreq() {
    switch (service.value) {
      case "Product":
        try {
          const res = await productService.updateProductImg(Id.value, onFileChange());
          uploadImg.value = false;
          await productStore.getProducts();
        } catch (e) {
          console.log(e)
        }
        break;
    }
  }

  return { EditImg, onFileChange, Sendreq, url, files, uploadImg }
})
