import { ref, watch, computed } from 'vue'
import { defineStore } from 'pinia'
import type Employee from '@/types/Employee'
import employeeService from '@/services/employee'

export const useEmployeeStore = defineStore('employee', () => {
  const dialog = ref(false);
  const search = ref("");
  const showEmployee = computed(() => {
    return employee.value.filter((item) => {
      return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })
  const employee = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({ name: "", tel: "", email: "", position: "", hourly_wage: 0 })

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedEmployee.value = { name: "", tel: "", email: "", position: "", hourly_wage: 0 }
    }
  })

  async function getEmployees() {
    try {
      const res = await employeeService.getEmployees();
      employee.value = res.data;
    } catch (e) {
      console.log(e)
    }
  }

  async function saveEmployees() {
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }
      dialog.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e)
    }
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  async function deleteEmployee(id: number) {
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e)
    }
  }

  return { employee, dialog, editedEmployee, getEmployees, saveEmployees, editEmployee, deleteEmployee, search, showEmployee }
})
