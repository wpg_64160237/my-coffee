import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Customer from '@/types/Customer'
import customerService from '@/services/customer'

export const useCustomerStore = defineStore('customer', () => {
  const dialog = ref(false);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({ name: "", tel: "", point: 0 })

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "", point: 0 }
    }
  });

  async function getCustomers() {
    try {
      const res = await customerService.getCustomers();
      customers.value = res.data;
    } catch (e) {
      console.log(e);
    }
  };

  async function saveCustomer() {
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value,
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }
      dialog.value = false;
      await getCustomers();
    } catch (e) {
      console.log(e);
    }
  }

  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }

  async function deleteCustomer(id: number) {
    try {
      const res = await customerService.deleteCustomer(id)
      await getCustomers();
    } catch (e) {
      console.log(e);
    }
  }

  const search = ref("");
  const showCustomer = computed(() => {
    return customers.value.filter((item) => {
      return (item.tel || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  })

  return { customers, dialog, editedCustomer, getCustomers, saveCustomer, editCustomer, deleteCustomer, search, showCustomer }
})
