import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Store from '@/types/Store';
import storeService from '@/services/store'

export const useStoreStore = defineStore('store', () => {
  const dialog = ref(false);
  const stores = ref<Store[]>([]);
  const editedStore = ref<Store>({ name: "", address: "", tel: "" });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedStore.value = { name: "", address: "", tel: "" };
    }
  });

  async function getStores() {
    try {
      const res = await storeService.getStores();
      stores.value = res.data
    } catch (e) {
      console.log(e);
    }
  };

  async function saveStore() {
    try {
      if (editedStore.value.id) {
        const res = await storeService.updateStore(
          editedStore.value.id,
          editedStore.value
        );
      } else {
        const res = await storeService.saveStore(editedStore.value);
      }
      dialog.value = false;
      await getStores();
    } catch (e) {
      console.log(e);
    }
  };

  function editStore(store: Store) {
    editedStore.value = JSON.parse(JSON.stringify(store));
    dialog.value = true;
  };

  async function deleteStore(id: number) {
    try {
      const res = await storeService.deleteStore(id)
      await getStores();
    } catch (e) {
      console.log(e)
    }
  };

  const search = ref("");
  const showStore = computed(() => {
    return stores.value.filter((item) => {
      return (item.name || '').toLowerCase().match((search.value || '').toLowerCase());
    })
  });

  return { stores, dialog, editedStore, getStores, saveStore, editStore, deleteStore, search, showStore }
})
